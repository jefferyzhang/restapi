package settings

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"
)

//App this for App config
type App struct {
	JwtSecret string
	PageSize  int
	PrefixURL string

	RuntimeRootPath string

	ImageSavePath  string
	ImageMaxSize   int
	ImageAllowExts []string

	ExportSavePath string
	QrCodeSavePath string
	FontSavePath   string

	LogSavePath string
	LogSaveName string
	LogFileExt  string
	TimeFormat  string
}

//Server is Server config
type Server struct {
	RunMode      string
	HTTPPort     int
	ReadTimeout  time.Duration
	WriteTimeout time.Duration
}

//Database is configuration for store DB, Now Support MySQL
type Database struct {
	Type        string
	User        string
	Password    string
	Host        string
	Name        string
	TablePrefix string
}

//Redis is Running DB
type Redis struct {
	Host        string
	Password    string
	MaxIdle     int
	MaxActive   int
	IdleTimeout time.Duration
}

//AppConfig is config file
type AppConfig struct {
	AppName    string    `json:"appanme"`
	Version    string    `json:"version"`
	Appconf    *App      `json:"app"`
	Serverconf *Server   `json:"server"`
	DBconf     *Database `json:"database"`
	RedisConf  *Redis    `json:"redis"`
}

var (
	// MyConfig is the default config for Application
	MyConfig *AppConfig
	// AppPath is the absolute path to the app
	AppPath string

	// appConfigPath is the path to the config files
	appConfigPath string
	// appConfigProvider is the provider for the config, default is json
	appConfigProvider = "json"
)

func init() {
	MyConfig = newAppConfig()
	var err error
	if AppPath, err = filepath.Abs(filepath.Dir(os.Args[0])); err != nil {
		panic(err)
	}

	var filename = "appconf.json"
	appConfigPath = filepath.Join(AppPath, "conf", filename)
	if err = parseConfig(appConfigPath); err != nil {
		//panic(err)
	}
}

func parseConfig(appConfigPath string) (err error) {
	// Open our jsonFile
	jsonFile, err := os.Open(appConfigPath)
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
		return err
	}
	fmt.Println("Successfully Opened config json.")
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()

	// read our opened xmlFile as a byte array.
	byteValue, err := ioutil.ReadAll(jsonFile)
	// we unmarshal our byteArray which contains our
	// jsonFile's content into 'users' which we defined above
	err = json.Unmarshal(byteValue, MyConfig)

	return err
}

func newAppConfig() *AppConfig {
	return &AppConfig{
		Version: "19.07.15.0",
		AppName: "Blog",
		Appconf: &App{
			PageSize:        10,
			JwtSecret:       "233",
			PrefixURL:       "http://127.0.0.1:8000",
			RuntimeRootPath: "runtime/",
			ImageSavePath:   "upload/images/",
			ImageMaxSize:    5,
			ImageAllowExts:  []string{".jpg", ".jpeg", ".png"},
			ExportSavePath:  "export/",
			QrCodeSavePath:  "qrcode/",
			FontSavePath:    "fonts/",
			LogSavePath:     "logs/",
			LogSaveName:     "log",
			LogFileExt:      "log",
			TimeFormat:      "20060102",
		},
		Serverconf: &Server{
			RunMode:      "debug",
			HTTPPort:     8082,
			ReadTimeout:  60,
			WriteTimeout: 60,
		},
		DBconf: &Database{
			Type:        "mysql",
			User:        "root",
			Password:    "20040413A",
			Host:        "10.1.1.155:3306",
			Name:        "blog",
			TablePrefix: "blog_",
		},
		RedisConf: &Redis{
			Host:        "10.1.1.155:6379",
			Password:    "",
			MaxIdle:     30,
			MaxActive:   30,
			IdleTimeout: 200,
		},
	}
}
