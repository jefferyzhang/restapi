package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"os"

	"bitbucket.org/jefferyzhang/restapi/middleware/token"
	"bitbucket.org/jefferyzhang/restapi/models"
	"bitbucket.org/jefferyzhang/restapi/routers"
	"bitbucket.org/jefferyzhang/restapi/settings"
	_ "bitbucket.org/jefferyzhang/restapi/settings"
	"bitbucket.org/jefferyzhang/restapi/utils/gredis"
	"github.com/gin-gonic/gin"

	_ "bitbucket.org/jefferyzhang/restapi/docs"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func init() {
	token.Setup()
	models.Setup()
	gredis.Setup()
}

// @title gin Server rest api
// @version 1.0
// @description This is a sample server Petstore server.
// @termsOfService http://swagger.io/terms/

// @contact.name API Support
// @contact.url http://facebook
// @contact.email zytiam@hotmail.com

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host petstore.swagger.io
// @BasePath /v1
func main() {
	fmt.Println("Hello world!!!!")
	var logger = log.New(os.Stdout, "AppData: ", log.Lshortfile)
	//jsonconf, err := conf.NewConfig("json", "./conf/appconf.json")
	//if err != nil {
	//	logger.Fatal(err)
	//}
	gin.SetMode(settings.MyConfig.Serverconf.RunMode)
	logger.Println(settings.MyConfig.AppName)
	fmt.Println(settings.AppPath)
	listport := fmt.Sprintf("0.0.0.0:%d", settings.MyConfig.Serverconf.HTTPPort)
	fmt.Println(fmt.Sprintf("Service Port: %s", listport))
	//logger.Println(jsonconf.String("version"))
	/*
		router := gin.Default()

		router.GET("/version", func(c *gin.Context) {
			c.JSON(200, gin.H{
				"message": "version",
			})
		})
		router.Run(listport)
		//*/
	///*
	server := &http.Server{
		Addr:    listport,
		Handler: routers.InitRouter(),
		//ReadTimeout:    settings.MyConfig.Serverconf.ReadTimeout,
		//WriteTimeout:   settings.MyConfig.Serverconf.WriteTimeout,
		//MaxHeaderBytes: 1 << 20,
	}
	//*/
	ln, err := net.Listen("tcp4", listport)
	if err != nil {
		log.Fatal(err)
	}

	if err := server.Serve(ln); /*server.ListenAndServe()*/ err != nil && err != http.ErrServerClosed {
		log.Fatalf("listen: %s\n", err)
	}

}
