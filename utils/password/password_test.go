package password

import (
	"testing"
)

func TestDefaults(t *testing.T) {
	password := "secret"
	hash, _ := HashPassword(password) // ignore error for the sake of simplicity

	t.Logf("Password: %v", password)
	t.Logf("Hash:    %v", hash)

	match := CheckPasswordHash(password, hash)
	if !match {
		t.Fatal("failed")
	}
	match = CheckPasswordHash("password", hash)
	if match {
		t.Fatal("failed")
	}
}
