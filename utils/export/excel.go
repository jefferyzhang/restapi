package export

import (
	"bitbucket.org/jefferyzhang/restapi/settings"
)

const EXT = ".xlsx"

// GetExcelFullUrl get the full access path of the Excel file
func GetExcelFullUrl(name string) string {
	return settings.MyConfig.Appconf.PrefixURL + "/" + GetExcelPath() + name
}

// GetExcelPath get the relative save path of the Excel file
func GetExcelPath() string {
	return settings.MyConfig.Appconf.ExportSavePath
}

// GetExcelFullPath Get the full save path of the Excel file
func GetExcelFullPath() string {
	return settings.MyConfig.Appconf.RuntimeRootPath + GetExcelPath()
}
