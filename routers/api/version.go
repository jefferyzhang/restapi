package api

import (
	"net/http"

	"bitbucket.org/jefferyzhang/restapi/utils/app"
	"bitbucket.org/jefferyzhang/restapi/utils/e"
	"github.com/gin-gonic/gin"
)

// GetVersion get server verison
// @Summary Get version
// @Produce  json
// @Success 200 {object} app.Response
// @Failure 500 {object} app.Response
// @Router /version [get]
func GetVersion(c *gin.Context) {
	appG := app.Gin{C: c}
	appG.Response(http.StatusOK, e.SUCCESS, map[string]string{
		"version": "0.0.2.3",
	})
}
